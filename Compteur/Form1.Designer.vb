﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fenetre_compteur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_reset_vb = New System.Windows.Forms.Button()
        Me.Btn_increase_vb = New System.Windows.Forms.Button()
        Me.Btn_decrease_vb = New System.Windows.Forms.Button()
        Me.nb_compteur_vb = New System.Windows.Forms.Label()
        Me.Lbl_total = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btn_reset_vb
        '
        Me.Btn_reset_vb.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Btn_reset_vb.Location = New System.Drawing.Point(144, 176)
        Me.Btn_reset_vb.Name = "Btn_reset_vb"
        Me.Btn_reset_vb.Size = New System.Drawing.Size(106, 45)
        Me.Btn_reset_vb.TabIndex = 2
        Me.Btn_reset_vb.Text = "RAZ"
        Me.Btn_reset_vb.UseVisualStyleBackColor = True
        '
        'Btn_increase_vb
        '
        Me.Btn_increase_vb.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Btn_increase_vb.Location = New System.Drawing.Point(254, 92)
        Me.Btn_increase_vb.Name = "Btn_increase_vb"
        Me.Btn_increase_vb.Size = New System.Drawing.Size(107, 45)
        Me.Btn_increase_vb.TabIndex = 3
        Me.Btn_increase_vb.Text = "+"
        Me.Btn_increase_vb.UseVisualStyleBackColor = True
        '
        'Btn_decrease_vb
        '
        Me.Btn_decrease_vb.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.Btn_decrease_vb.Location = New System.Drawing.Point(31, 92)
        Me.Btn_decrease_vb.Name = "Btn_decrease_vb"
        Me.Btn_decrease_vb.Size = New System.Drawing.Size(106, 44)
        Me.Btn_decrease_vb.TabIndex = 4
        Me.Btn_decrease_vb.Text = "-"
        Me.Btn_decrease_vb.UseVisualStyleBackColor = True
        '
        'nb_compteur_vb
        '
        Me.nb_compteur_vb.AutoSize = True
        Me.nb_compteur_vb.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold)
        Me.nb_compteur_vb.Location = New System.Drawing.Point(180, 92)
        Me.nb_compteur_vb.Name = "nb_compteur_vb"
        Me.nb_compteur_vb.Size = New System.Drawing.Size(36, 37)
        Me.nb_compteur_vb.TabIndex = 5
        Me.nb_compteur_vb.Text = "0"
        '
        'Lbl_total
        '
        Me.Lbl_total.AutoSize = True
        Me.Lbl_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Lbl_total.Location = New System.Drawing.Point(167, 40)
        Me.Lbl_total.Name = "Lbl_total"
        Me.Lbl_total.Size = New System.Drawing.Size(59, 17)
        Me.Lbl_total.TabIndex = 6
        Me.Lbl_total.Text = "TOTAL"
        '
        'fenetre_compteur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 254)
        Me.Controls.Add(Me.Lbl_total)
        Me.Controls.Add(Me.nb_compteur_vb)
        Me.Controls.Add(Me.Btn_decrease_vb)
        Me.Controls.Add(Me.Btn_increase_vb)
        Me.Controls.Add(Me.Btn_reset_vb)
        Me.Name = "fenetre_compteur"
        Me.Text = "Basic Counter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_reset_vb As Button
    Friend WithEvents Btn_increase_vb As Button
    Friend WithEvents Btn_decrease_vb As Button
    Friend WithEvents nb_compteur_vb As Label
    Friend WithEvents Lbl_total As Label
End Class

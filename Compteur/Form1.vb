﻿Imports Class_compteur


Public Class fenetre_compteur

    Dim compteur As New ClassCompteur

    Private Sub Btn_increase(sender As Object, e As EventArgs) Handles Btn_increase_vb.Click
        compteur.incrementation()

        nb_compteur_vb.Text = compteur.get_nbr()
    End Sub

    Private Sub Btn_decrease(sender As Object, e As EventArgs) Handles Btn_decrease_vb.Click
        compteur.decrementation()

        nb_compteur_vb.Text = compteur.get_nbr()

    End Sub

    Private Sub Btn_reset(sender As Object, e As EventArgs) Handles Btn_reset_vb.Click
        compteur.remise_a_zero()

        nb_compteur_vb.Text = compteur.get_nbr()

    End Sub

End Class

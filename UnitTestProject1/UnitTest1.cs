﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Class_compteur
{
    [TestClass]
    public class Compteur_Test
    {
        [TestMethod]
        public void Test_set_nbr()
        {
            ClassCompteur Compteur_Test = new ClassCompteur();
            Compteur_Test.set_nbr(50);
            Assert.AreEqual(50, Compteur_Test.get_nbr());
        }


        [TestMethod]
        public void Test_get_nbr()
        {
            ClassCompteur Compteur_Test = new ClassCompteur();
            Assert.AreEqual(0, Compteur_Test.get_nbr());
        }


        [TestMethod]
        public void Test_incrementation()
        {
            ClassCompteur Compteur_Test = new ClassCompteur();
            Compteur_Test.incrementation(); 
            Assert.AreEqual(1, Compteur_Test.get_nbr());              
        }


        [TestMethod]
        public void Test_decrementation()
        {
            ClassCompteur Compteur_Test = new ClassCompteur();
            Compteur_Test.incrementation();
            Compteur_Test.decrementation(); 
            Assert.AreEqual(0, Compteur_Test.get_nbr());
            Compteur_Test.decrementation();
            Assert.AreEqual(0, Compteur_Test.get_nbr());
        }


        [TestMethod]
        public void Test_remise_a_zero()
        {
            ClassCompteur Compteur_Test = new ClassCompteur();
            Compteur_Test.set_nbr(26546);
            Compteur_Test.remise_a_zero();
            Assert.AreEqual(0, Compteur_Test.get_nbr());
        }



    }
}


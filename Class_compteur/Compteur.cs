﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_compteur
{
    public class ClassCompteur
    {

        static private int nb_compteur;
        


        public ClassCompteur()
        {
            nb_compteur = 0; 
        }

        public int get_nbr() { return nb_compteur;  }
        public void set_nbr(int new_nbr) { nb_compteur = new_nbr; }
        public void incrementation()
        {
            nb_compteur++; 
        } 
        public void decrementation()
        {
            if (nb_compteur > 0)
                nb_compteur--;
            else
                nb_compteur = 0;
        }
        public void remise_a_zero()
        {
            nb_compteur = 0; 
        }
        
     
    }
}
